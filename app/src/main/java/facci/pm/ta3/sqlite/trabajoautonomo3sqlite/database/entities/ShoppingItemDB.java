package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.widget.DialogTitle;

import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.MainActivity;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.adapter.ShoppingItemAdapter;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }


    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos

        // Se crea una instancia de la clase para conseguir el repositorio de la base de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // se procede a crear un objeto de la clase Contentvalues que va a ser un nuevo objeto la misma clase Contentvalue
        // siendo de mapeo para los valores donde los nombres de la columna son las claves
        ContentValues values = new ContentValues();

        //  aqui se van a guardar los valores de la base de datos
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,productName);


        //se van a insertar en la tabla TABLE_NAME todos los valores que se han guardado en el objeto values
        db.insert(ShoppingElementEntry.TABLE_NAME, null, values);

    }


    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }

    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos

        // Se crea una instancia de la clase para conseguir el repositorio de la base de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //Se procede a eliminar la base de datos encontrados en el dbHelper
        db.delete(ShoppingElementEntry.TABLE_NAME,null,null);

    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos

        //Se crea una instancia de la clase para conseguir el repositorio de la base de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // se procede a crear un objeto de la clase Contentvalues que va a ser un nuevo objeto la misma clase Contentvalue
        // siendo de mapeo para los valores donde los nombres de la columna son las claves
        ContentValues values = new ContentValues();

        //guardar el nombre del producto
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName());

        // se actualiza la lista que esten presente en con el nombre del producto
        String selection = ShoppingElementEntry._ID + " = ?";

        // se procede a detallar los productos en orden segun su posicion
        String[] selectionArgs = {String.valueOf(shoppingItem.getId())};

        // se actualiza la lista en la tabla
        int updateRows = db.update(ShoppingElementEntry.TABLE_NAME,values,selection,selectionArgs);
    }

    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos

        //Se crea una instancia de la clase para conseguir el repositorio de la base de datos en modo escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Se crea un buscador para la consulta de la lista de los productos
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " LIKE ?";

        // se procede a detallar los productos en orden segun su nombre
        String[] selectionArgs = {(shoppingItem.getName())};

        // se procede a eliminar el elemento de la lista
      int DeleteRows =  db.delete(ShoppingElementEntry.TABLE_NAME,selection,selectionArgs);


    }
}
